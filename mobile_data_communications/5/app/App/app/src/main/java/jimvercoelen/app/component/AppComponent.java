package jimvercoelen.app.component;

import javax.inject.Singleton;

import dagger.Component;
import jimvercoelen.app.activities.MainActivity;
import jimvercoelen.app.modules.AppModule;

@Singleton
@Component(
  modules = {
    AppModule.class
  }
)
public interface AppComponent {
  void inject(MainActivity activity);
}
