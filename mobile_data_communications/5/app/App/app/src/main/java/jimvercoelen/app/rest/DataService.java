package jimvercoelen.app.rest;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataService {

  private DataService() {}

  public static DataApi createDataService() {
    Retrofit.Builder builder = new Retrofit.Builder();
    builder.baseUrl("http://10.0.2.1:8080");
    builder.addConverterFactory(GsonConverterFactory.create());
    builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());

    return builder.build().create(DataApi.class);
  }
}
