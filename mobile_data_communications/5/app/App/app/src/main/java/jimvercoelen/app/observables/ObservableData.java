package jimvercoelen.app.observables;

import java.util.List;

import jimvercoelen.app.modals.Contact;
import jimvercoelen.app.rest.DataApi;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public class ObservableData {
  public ObservableData() {}

  public Observable<List<Contact>> getData(DataApi client) {
    BehaviorSubject<List<Contact>> requestSubject = BehaviorSubject.create();

    client.getData()
      .subscribeOn(Schedulers.io())
      .observeOn(Schedulers.io())
      .subscribe(
        requestSubject::onNext,
        requestSubject::onError,
        requestSubject::onCompleted
      );

    return  requestSubject.asObservable();
  }
}
