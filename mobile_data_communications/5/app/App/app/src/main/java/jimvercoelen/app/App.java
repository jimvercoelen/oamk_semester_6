package jimvercoelen.app;

import android.app.Application;
import android.content.Context;

import jimvercoelen.app.component.AppComponent;
import jimvercoelen.app.component.DaggerAppComponent;
import jimvercoelen.app.modules.AppModule;

public class App extends Application {
  private AppComponent appComponent;

  @Override
  public void onCreate() {
    super.onCreate();

    appComponent = DaggerAppComponent.builder()
      .appModule(new AppModule(this))
      .build();
  }

  public static AppComponent getComponent(Context context) {
    return ((App) context.getApplicationContext()).appComponent;
  }
}
