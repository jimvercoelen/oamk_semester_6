package jimvercoelen.app.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jimvercoelen.app.observables.ObservableData;
import jimvercoelen.app.rest.DataApi;
import jimvercoelen.app.rest.DataService;

@Module
public final class AppModule {
  Application application;

  public AppModule(Application application) {
    this.application = application;
  }

  @Provides @Singleton
  Application provideApplication() {
    return this.application;
  }

  @Provides @Singleton
  DataApi provideClient() {
    return DataService.createDataService();
  }

  @Provides @Singleton
  ObservableData provideObservableData() {
    return new ObservableData();
  }
}
