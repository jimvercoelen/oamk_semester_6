package jimvercoelen.app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import jimvercoelen.app.App;
import jimvercoelen.app.R;
import jimvercoelen.app.modals.Contact;
import jimvercoelen.app.observables.ObservableData;
import jimvercoelen.app.rest.DataApi;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = MainActivity.class.getName();

  @Inject DataApi client;
  @Inject ObservableData observableData;
  @BindView(R.id.list_contacts) ListView contactsListView;
  @BindView(R.id.progressbar) ProgressBar progressBar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    App.getComponent(this).inject(this);
    ButterKnife.bind(this);

    getData();
  }

  private void getData() {
    progressBar.setVisibility(View.VISIBLE);

    observableData
      .getData(client)
      .subscribeOn(Schedulers.newThread())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onSuccess,
        this::onError,
        this::onComplete
      );
  }

  private void onSuccess(List<Contact> contacts) {
    List<String> contactList = new ArrayList<>();

    for (Contact contact : contacts) {
      Log.i(TAG, " received contact:  " + contact.toString());
      contactList.add(contact.toString());
    }

    ArrayAdapter<String> listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, contactList);
    contactsListView.setAdapter(listAdapter);
  }

  private void onError(Throwable error) {
    Log.e(TAG, error.getMessage());
    Log.getStackTraceString(error);
  }

  private void onComplete() {
    Log.i(TAG, "Completed getting data");
    progressBar.setVisibility(View.GONE);
  }
}
