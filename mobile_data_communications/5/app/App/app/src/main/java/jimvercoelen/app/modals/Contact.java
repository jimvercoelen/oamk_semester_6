package jimvercoelen.app.modals;

public class Contact {
  private String id;
  private String name;
  private String email;
  private String address;
  private String gender;
  private Phone phone;

  public Contact(String id, String name, String email, String address, String gender, Phone phone) {
    this.id =id;
    this.name=  name;
    this.email = email;
    this.address = address;
    this.gender = gender;
    this.phone = phone;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getEmail() {
    return email;
  }

  public String getAddress() {
    return address;
  }

  public String getGender() {
    return gender;
  }

  public Phone getPhone() {
    return phone;
  }

  @Override
  public String toString() {
    return "id: " + id
      + "\nname:  " + name
      + "\nemail: " + email
      + "\naddress: " + address
      + "\ngender: " + gender;
  }

  private class Phone {
    private String mobile;
    private String home;
    private String office;

    public Phone(String mobile, String home, String office) {
      this.mobile = mobile;
      this.home = home;
      this.office = office;
    }

    public String getMobile() {
      return mobile;
    }

    public String getHome() {
      return home;
    }

    public String getOffice() {
      return office;
    }
  }
}
