package jimvercoelen.app.rest;

import java.util.List;

import jimvercoelen.app.modals.Contact;
import retrofit2.http.GET;
import rx.Observable;

public interface DataApi {
  @GET("/data")
  Observable<List<Contact>> getData();
}
