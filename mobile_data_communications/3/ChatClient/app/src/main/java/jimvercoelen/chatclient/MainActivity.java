package jimvercoelen.chatclient;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
  static final String TAG = MainActivity.class.getName();
  static final String NICKNAME = "Ossi Oppilas";
  static final String SERVER_IP_ADDRESS = "172.20.240.4";

  private InetAddress serverAddress;
  private Socket socket;
  private CommunicationThread communicationThread;

  static TextView receivedMessaged;
  @BindView(R.id.text_message_input) EditText input;

  static public Handler UIupdater = new Handler() {
    @Override
    public void handleMessage(Message message) {
      int numOfBytesReceived = message.arg1;
      byte[] buffer = (byte[]) message.obj;


      String receivedMessage = new String(buffer);
      receivedMessage = receivedMessage.substring(0, numOfBytesReceived);

      receivedMessaged.setText(receivedMessaged.getText().toString() + receivedMessage);
    }
  };

  private class CreateCommThreadTask extends AsyncTask<Void, Integer, Void> {
    @Override
    protected Void doInBackground(Void... params) {
      try {
        serverAddress = InetAddress.getByName(SERVER_IP_ADDRESS);
        socket = new Socket(serverAddress, 7001); //IP, PORT NUMBER
        communicationThread = new CommunicationThread(socket);
        communicationThread.start();
        sendToServer(NICKNAME);
        //
      } catch (UnknownHostException e) {
        Log.d(TAG, e.getLocalizedMessage());
      } catch (IOException e) {
        Log.d(TAG, e.getLocalizedMessage());
      }
      return null;
    }
  }

  private class WriteToServerTask extends AsyncTask<byte[], Void, Void> {
    protected Void doInBackground(byte[]... data) {
      communicationThread.write(data[0]);
      return null;
    }
  }

  private class CloseSocketTask extends AsyncTask<Void, Void, Void> {
    @Override
    protected Void doInBackground(Void... params) {
      try {
        socket.close();
      } catch (IOException e) {
        Log.d(TAG, e.getLocalizedMessage());
      }

      return null;
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    receivedMessaged = (TextView) findViewById(R.id.text_message_received);

    ButterKnife.bind(this);
  }

  @Override
  public void onResume() {
    super.onResume();
    new CreateCommThreadTask().execute();
  }

  @Override
  public void onPause() {
    super.onPause();
    new CloseSocketTask().execute();
  }

  @OnClick(R.id.button_send_message)
  public void onSendMessageClick(View view) {
    sendToServer(input.getText().toString());
  }

  private void sendToServer(String message) {
    byte[] theByteArray = message.getBytes();
    new WriteToServerTask().execute(theByteArray);
  }
}
