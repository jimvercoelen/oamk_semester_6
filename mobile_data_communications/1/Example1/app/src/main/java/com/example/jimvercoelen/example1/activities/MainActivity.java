package com.example.jimvercoelen.example1.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.jimvercoelen.example1.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.et_car) EditText etCar;
    @BindView(R.id.lv_cars) ListView lvCars;

    private static List<String> cars = new ArrayList<>();
    private ArrayAdapter<String> adapter = null;

    static {
        cars.add("Alfa");
        cars.add("Audi");
        cars.add("Opel");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cars);
        lvCars.setAdapter(adapter);
    }

    @OnClick(R.id.btn_add)
    public void onAdd(View view) {
        String car = etCar.getText().toString();
        adapter.add(car);
        etCar.getText().clear();
    }

    @OnClick(R.id.btn_edit)
    public void onEdit(View view) {
        lvCars.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedCar = cars.get(i);
                etCar.setText(selectedCar);
            }
        });
    }

    @OnClick(R.id.btn_remove)
    public void onRemove(View view) {
        lvCars.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String car = cars.get(i);
                adapter.remove(car);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @OnClick(R.id.btn_2e_activity)
    public void onNext(View view) {
        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
        MainActivity.this.startActivity(intent);
    }
}
