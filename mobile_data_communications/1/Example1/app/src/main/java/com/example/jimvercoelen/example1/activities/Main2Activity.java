package com.example.jimvercoelen.example1.activities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.jimvercoelen.example1.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_back)
    public void onBack(View view) {
        this.finish();
    }
}
