package jimvercoelen.httpapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = MainActivity.class.getName();
  private static final String URL = "http://172.20.240.4:7002/";
  private static final String KEY_MATCH = "match";
  private static final String KEY_HOME_TEAM = "home_team";
  private static final String KEY_VISITOR_TEAM = "visitor_team";
  private static final String KEY_HOME_GOALS = "home_goals";
  private static final String KEY_VISITOR_GOALS = "visitor_goals";

  @BindView(R.id.list_items) ListView itemsListView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);

    this.fetchData();
  }

  private void fetchData () {
    new Thread(() -> {
      List<String> list = new ArrayList<>();
      String xml = XMLParser.getXmlFromApi(URL);
      Document doc = XMLParser.getDomElement(xml);
      NodeList nl = doc.getElementsByTagName(KEY_MATCH);

      for (int i = 0; i < nl.getLength(); i++) {
        Element e = (Element) nl.item(i);
        StringBuilder builder = new StringBuilder();

        builder.append(KEY_HOME_TEAM + " " + XMLParser.getValue(e, KEY_HOME_TEAM) + "\n");
        builder.append(KEY_VISITOR_TEAM + " " + XMLParser.getValue(e, KEY_VISITOR_TEAM) + "\n");
        builder.append(KEY_HOME_GOALS + " " + XMLParser.getValue(e, KEY_HOME_GOALS) + "\n");
        builder.append(KEY_VISITOR_GOALS + " " + XMLParser.getValue(e, KEY_VISITOR_GOALS) + "\n ");

        list.add(builder.toString());
      }

      ArrayAdapter<String> listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
      runOnUiThread(() -> itemsListView.setAdapter(listAdapter));

    }).start();
  }
}
