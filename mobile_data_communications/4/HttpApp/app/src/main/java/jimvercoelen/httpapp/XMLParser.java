package jimvercoelen.httpapp;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XMLParser {

  public static String getXmlFromApi(String url) {
    String xml = null;

    try {
      DefaultHttpClient httpClient = new DefaultHttpClient();
      HttpPost request = new HttpPost(url);

      HttpResponse response = httpClient.execute(request);
      HttpEntity entity = response.getEntity();
      xml = EntityUtils.toString(entity);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return xml;
  }

  public static Document getDomElement(String xml){
    Document doc = null;
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    try {

      DocumentBuilder db = dbf.newDocumentBuilder();

      InputSource is = new InputSource();
      is.setCharacterStream(new StringReader(xml));
      doc = db.parse(is);

    } catch (ParserConfigurationException | SAXException | IOException e) {
      Log.e("Error: ", e.getMessage());
    }

    return doc;
  }

  public static String getValue(Element item, String str) {
    NodeList n = item.getElementsByTagName(str);

    return getElementValue(n.item(0));
  }

  private static String getElementValue(Node elem) {
    Node child;
    if (elem != null) {
      if (elem.hasChildNodes()) {
        for (child = elem.getFirstChild(); child != null; child = child.getNextSibling()){
          if (child.getNodeType() == Node.TEXT_NODE){
            return child.getNodeValue();
          }
        }
      }
    }

    return "";
  }
}
