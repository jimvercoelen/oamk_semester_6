//
//  ViewController.swift
//  Calculator
//
//  Created by Jim Vercoelen on 05/10/2017.
//  Copyright © 2017 Jim Vercoelen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var labelDisplay: UILabel!
    var firstInput: Double = 0
    var secondInput : Double = 0
    var result: Double = 0
    var calculaterFeature = CalculaterFeatures.UNDEFINED
    
    enum CalculaterFeatures {
        case UNDEFINED
        case ADD
        case SUBTRACT
        case MULTIPLY
        case DIVIDE
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func handleAddClick(_ sender: UIButton) {
        calculaterFeature = CalculaterFeatures.ADD
        firstInput = Double((labelDisplay?.text)!)!
        labelDisplay.text = "0"
        
        print(firstInput)
    }
    
    @IBAction func handleSubtractClick(_ sender: UIButton) {
        calculaterFeature = CalculaterFeatures.SUBTRACT
        firstInput = Double((labelDisplay?.text)!)!
        labelDisplay.text = "0"
        
        print(firstInput)
    }
    
    @IBAction func handleMultiplyClick(_ sender: UIButton) {
        calculaterFeature = CalculaterFeatures.MULTIPLY
        firstInput = Double((labelDisplay?.text)!)!
        labelDisplay.text = "0"
        
        print(firstInput)
    }
   
    @IBAction func handleDivideClick(_ sender: UIButton) {
        calculaterFeature = CalculaterFeatures.DIVIDE
        firstInput = Double((labelDisplay?.text)!)!
        labelDisplay.text = "0"
        
        print(firstInput)
    }
    
    @IBAction func handleClearClick(_ sender: UIButton) {
        firstInput = 0
        secondInput = 0
        labelDisplay.text = "0"
    }
 
    @IBAction func handleCalculateClick(_ sender: UIButton) {
        let input: Double = Double((labelDisplay?.text)!)!
        
        if input != result {
            secondInput = input
        } else {
            firstInput = input
        }
        
        if calculaterFeature == CalculaterFeatures.ADD {
            result = firstInput + secondInput
        } else if calculaterFeature == CalculaterFeatures.SUBTRACT {
            result = firstInput - secondInput
        } else if calculaterFeature == CalculaterFeatures.MULTIPLY {
            result = firstInput * secondInput
        } else if calculaterFeature == CalculaterFeatures.DIVIDE {
            result = firstInput / secondInput
        }
        
        if result > 9999999 {
            result = 9999999
        }
        
        print(secondInput, firstInput, calculaterFeature, result)
        labelDisplay.text = String(result)
    }
    
    @IBAction func handleButtonPress(_ sender: UIButton) {
        let number = Int((sender.titleLabel?.text)!)!
        
        print(number)
        
        updateLabel(number: number)
    }
    
    func updateLabel(number: Int) {
        if let text = labelDisplay.text {
            if text == "0" {
                labelDisplay.text = String(number)
            } else {
                labelDisplay.text = String(text) + String(number)
            }
        }
    }
    
    func doubleToString(number: Double) -> String {
        let isInteger = floor(number) == number
        
        if isInteger {
            return String(format: "%d", number)
        } else {
            return String(format: "%.2d", number)
        }
    }
}

