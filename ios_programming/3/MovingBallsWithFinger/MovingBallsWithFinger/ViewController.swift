//
//  ViewController.swift
//  MovingBallsWithFinger
//
//  Created by kari on 11/11/2016.
//  Copyright © 2016 kari. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   override func viewDidLoad()
   {
      super.viewDidLoad()
      
      // The property 'view' refers to the View that is controlled
      // by this class. Here we do a forced cast to a subclass type
      // in order to call the method that creates the balls.
      
      ( view as! MovingBallsWithFingersView ).create_balls()
      
   }

   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }


}

