//
//  MovingBallsWithFingersView.swift Copyright (c) Kari Laitinen.
//  MovingBallsWithFinger
//
//  Created by kari on 09/09/14. (Objective-C)
//  2015-04-30 Swift version created by Pekka Liukko
//  2015-10-17 Modifications for Xcode 7, Swift 2
//  2016-11-11 Modifications for Swift 3. Last modification.


import UIKit

class MovingBallsWithFingersView: UIView
{
   var first_ball: Ball! = nil
   var second_ball: Ball! = nil
   var third_ball: Ball! = nil
   var ball_being_moved: Ball! = nil
   
   // The method that creates the balls will be called from the
   // ViewContoller when this view has been loaded.
   
   func create_balls()
   {
      if( first_ball == nil )
      {
         let view_width = bounds.size.width
         let view_height = bounds.size.height
         
         first_ball = Ball( view_width / 2, view_height / 2 - 150,
                            UIColor.red.cgColor )
         
         second_ball = Ball( view_width / 2, view_height / 2,
                             UIColor.green.cgColor )
         
         third_ball = Ball( view_width / 2, view_height / 2 + 150,
                            UIColor.blue.cgColor )
      }
   }

   override func touchesBegan( _ touches: Set<UITouch>?,
                              with event: UIEvent?)
   {
      if let touch = touches?.first as UITouch!
      {     
         let finger_position:CGPoint = touch.location(in: self)

         if ( first_ball.contains_point( finger_position )  )
         {
            ball_being_moved  =  first_ball
            ball_being_moved.activate_ball()
         }
         else if ( second_ball.contains_point( finger_position )  )
         {
            ball_being_moved  =  second_ball
            ball_being_moved.activate_ball()
         }
         else if ( third_ball.contains_point( finger_position )  )
         {
            ball_being_moved  =  third_ball
            ball_being_moved.activate_ball()
         }
         
         self.setNeedsDisplay()
         
      }
   }
   
   override func touchesMoved(_ touches: Set<UITouch>?,
                              with event: UIEvent?)
   {
      if ( ball_being_moved  !=  nil )
      {
         if let touch = touches?.first as UITouch!
         {
            let new_finger_position:CGPoint = touch.location( in: self )
            
            let previous_finger_position:CGPoint = touch.previousLocation( in: self )

            
            let finger_movement_x  =  new_finger_position.x
                                               -  previous_finger_position.x
            
            let finger_movement_y  =  new_finger_position.y
                                               -  previous_finger_position.y
            
            ball_being_moved.move_this_ball( finger_movement_x,
                                             finger_movement_y )
            
            self.setNeedsDisplay() // "repaint" the view
         }
      }
   }
   
   // In October 2015 we noticed that it is the touchesCanceled() that
   // works here.
   
   override func touchesCancelled( _ touches: Set<UITouch>?,
                                  with event: UIEvent? )
   {
      if ( ball_being_moved  !=  nil )
      {
         ball_being_moved.deactivate_ball()
         ball_being_moved = nil
         
         self.setNeedsDisplay() // "repaint" the view
      }
   }
   
   override func touchesEnded(_ touches: Set<UITouch>?,
                                with event: UIEvent? )
   {
      if ( ball_being_moved  !=  nil )
      {
         ball_being_moved.deactivate_ball()
         ball_being_moved = nil
         
         self.setNeedsDisplay() // "repaint" the view
      }
   }

   override func draw(_ rect: CGRect)
   {   
      let context: CGContext = UIGraphicsGetCurrentContext()!
      
      first_ball.draw( context )
      second_ball.draw( context )
      third_ball.draw( context )
   }
}
