import UIKit

class GesturesDemoView : UIView
{
    var balls_on_screen : [ Ball ] = [ ]
    var text_lines_to_screen = [ "With this application, you can",
                                 "explore what gesture events",
                                 "take place when you play",
                                 "with the touch screen",
                                 " " ]
    
    var ball_colors : [ UIColor ] =
        [ UIColor.red, UIColor.green, UIColor.yellow,
          UIColor.cyan, UIColor.magenta, UIColor.blue,
          UIColor.brown, UIColor.gray, UIColor.orange ]
    
    @IBAction func tap_detected(recognizer:UITapGestureRecognizer)
    {
        
        text_lines_to_screen.append("Tap")
        let tapped_point = recognizer.location( in: self )
        
        balls_on_screen.append(Ball(tapped_point.x, tapped_point.y, ball_colors[0].cgColor))
        
        let color_for_new_ball = ball_colors.removeLast()
        ball_colors.insert( color_for_new_ball, at: 0 )
        
        setNeedsDisplay() // "repaint" the view
    }
    
    @IBAction func swipe_detected(recognizer:UISwipeGestureRecognizer)
    {
        
        text_lines_to_screen.append("Swipe")
        
        setNeedsDisplay() // "repaint" the view
    }
    
    @IBAction func long_press_detected(recognizer:UILongPressGestureRecognizer)
    {
        
        text_lines_to_screen.append("Long Press")
        
        setNeedsDisplay() // "repaint" the view
    }
    
    @IBAction func pan_detected(recognizer:UIPanGestureRecognizer)
    {
        let pan_point = recognizer.location( in: self )
        var ball_index = balls_on_screen.count
        var ball_to_delete_is_found = false
        
        while ball_index > 0 && ball_to_delete_is_found == false
        {
            ball_index -= 1
            if balls_on_screen[ ball_index ].contains_point( pan_point )
            {
                balls_on_screen.remove(at: ball_index)
                ball_to_delete_is_found = true
            }
        }

        
        text_lines_to_screen.append("Pan")
        
        setNeedsDisplay() // "repaint" the view
    }
    
    @IBAction func rotation_detected(recognizer:UIRotationGestureRecognizer)
    {
        
        text_lines_to_screen.append("Rotation")
        
        setNeedsDisplay() // "repaint" the view
    }
    
    @IBAction func pinch_detected(recognizer:UIPinchGestureRecognizer)
    {
        let scale = recognizer.scale
        let shouldGrow = scale > 1
        
        for ball in balls_on_screen
        {
            if shouldGrow
            {
                ball.enlarge()
            }
            else
            {
                ball.shrink()
            }
        }
        
        text_lines_to_screen.append("Pinch")
        
        setNeedsDisplay() // "repaint" the view
    }
    
    @IBAction func screen_edge_pan_detected(recognizer:UIScreenEdgePanGestureRecognizer)
    {
        
        text_lines_to_screen.append("Screen Edge Pan")
        
        setNeedsDisplay() // "repaint" the view
    }
    
    override func draw(_ rect: CGRect)
    {
        // set the text color to black
        let fieldColor: UIColor = UIColor.black
        
        // set the font to Palatino-Roman 14
        let font = UIFont(name: "Palatino-Roman", size:14.0)
        
        // set the line spacing to 6
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.lineSpacing = 6.0
        
        // set the Obliqueness to 0.1
        let skew = 0.0
        
        let attributes: NSDictionary = [
            NSForegroundColorAttributeName: fieldColor,
            NSParagraphStyleAttributeName: paraStyle,
            NSObliquenessAttributeName: skew,
            NSFontAttributeName: font!
        ]
        
        
        // We'll delete the oldest lines from the array if necessary.
        while ( text_lines_to_screen.count > 10 )
        {
            text_lines_to_screen.remove( at: 0 )
        }
        
        
        for line_index in 0 ..< text_lines_to_screen.count
        {
            ( text_lines_to_screen[line_index] as NSString).draw(
                in: CGRect( x: 20.0, y: CGFloat(40 + 20 * line_index),
                            width: 300.0, height: 48.0),
                withAttributes: attributes as? [String : AnyObject])
        }
        
        let context: CGContext = UIGraphicsGetCurrentContext()!
        
        
        for ball in balls_on_screen
        {
            ball.draw( context )
        }
        
        
    }
    
}

