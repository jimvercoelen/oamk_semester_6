
//  MainViewController.swift
//  ChangingViewsNoIB

//  Created by kari on 2017-05-07.
//  Copyright © 2017 Kari Laitinen. All rights reserved.
//  2017-09-11 Last modification.

/* This app is created without the Interface Builder. The graphical
 tools for creating the user interface were not used.
 
 When the project was created:
 Main.storyboard was deleted
 Main interface field was emptied.
 
 */


import UIKit

class MainViewController: UIViewController
{
    var button_to_change_view : UIButton!
    var button_demo_button : UIButton!
    
    let another_view_controller = AnotherViewController()
    let button_demo_view_controller = ButtonDemoViewController()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        set_up_view()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func set_up_view()
    {
        view.backgroundColor = UIColor.lightGray
        
        button_to_change_view = UIButton( type: .system )
        button_to_change_view.setTitle( "CHANGE", for: .normal )
        button_to_change_view.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 24 )
        button_to_change_view.translatesAutoresizingMaskIntoConstraints = false
        button_to_change_view.layer.cornerRadius = 10
        button_to_change_view.clipsToBounds = true
        button_to_change_view.backgroundColor = UIColor.red
        button_to_change_view.setTitleColor( UIColor.white, for: .normal )
        button_to_change_view.addTarget( nil, action: #selector( MainViewController.change_button_pressed ),
                                         for: .touchUpInside )
        button_to_change_view.widthAnchor.constraint( equalToConstant: 192 ).isActive = true
        button_to_change_view.heightAnchor.constraint( equalToConstant: 96 ).isActive = true
        
        button_demo_button = UIButton( type: .system )
        button_demo_button.setTitle( "ButtonDemo", for: .normal )
        button_demo_button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 24 )
        button_demo_button.translatesAutoresizingMaskIntoConstraints = false
        button_demo_button.layer.cornerRadius = 10
        button_demo_button.clipsToBounds = true
        button_demo_button.backgroundColor = UIColor.green
        button_demo_button.setTitleColor( UIColor.white, for: .normal )
        button_demo_button.addTarget( nil, action: #selector( MainViewController.demo_button_pressed ),
                                         for: .touchUpInside )
        button_demo_button.widthAnchor.constraint( equalToConstant: 192 ).isActive = true
        button_demo_button.heightAnchor.constraint( equalToConstant: 96 ).isActive = true
        
        view.addSubview( button_to_change_view )
        view.addSubview( button_demo_button )
        
        button_to_change_view.centerXAnchor.constraint( equalTo: view.layoutMarginsGuide.centerXAnchor ).isActive = true
        button_to_change_view.centerYAnchor.constraint( equalTo: view.layoutMarginsGuide.centerYAnchor ).isActive = true
        
        button_demo_button.centerXAnchor.constraint( equalTo: view.layoutMarginsGuide.centerXAnchor ).isActive = true
        button_demo_button.centerYAnchor.constraint( equalTo: view.layoutMarginsGuide.centerYAnchor, constant: 112 ).isActive = true
    }
    
    func change_button_pressed()
    {
        
        another_view_controller.modalTransitionStyle = .coverVertical
        another_view_controller.modalPresentationStyle = .formSheet
        
        present( another_view_controller, animated: true, completion: nil )
    }
    
    func demo_button_pressed()
    {
        
        button_demo_view_controller.modalTransitionStyle = .coverVertical
        button_demo_view_controller.modalPresentationStyle = .formSheet
        
        present( button_demo_view_controller, animated: true, completion: nil )
    }
    
    override func viewDidDisappear( _ animated: Bool )
    {
        super.viewDidDisappear( animated  )
        print( "GRAY view did disappear. " )
    }
}

