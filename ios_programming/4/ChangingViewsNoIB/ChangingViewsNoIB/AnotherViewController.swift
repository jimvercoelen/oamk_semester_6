//
//  AnotherViewController.swift
//  ChangingViewsNoIB
//
//  Created by kari on 07/05/2017.
//  Copyright © 2017 Kari Laitinen. All rights reserved.
//  2017-09-11 Last modification.

import UIKit

class AnotherViewController: UIViewController
{
    var back_button : UIButton!
    var button_demo_button : UIButton!
    let button_demo_view_controller = ButtonDemoViewController()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        set_up_view()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func set_up_view()
    {
        view.backgroundColor = UIColor.cyan
        
        back_button = UIButton( type: .system )
        
        back_button.setTitle( "BACK", for: .normal )
        back_button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 36 )
        back_button.translatesAutoresizingMaskIntoConstraints = false
        back_button.layer.cornerRadius = 10 // This rounds the corners
        back_button.clipsToBounds = true    // This is needed as well
        back_button.backgroundColor = UIColor.blue
        back_button.setTitleColor( UIColor.yellow, for: .normal )
        back_button.addTarget( nil, action: #selector( AnotherViewController.back_button_pressed ), for: .touchUpInside )
        back_button.widthAnchor.constraint( equalToConstant: 192 ).isActive = true
        back_button.heightAnchor.constraint( equalToConstant: 96 ).isActive = true
        
        button_demo_button = UIButton( type: .system )
        button_demo_button.setTitle( "ButtonDemo", for: .normal )
        button_demo_button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 24 )
        button_demo_button.translatesAutoresizingMaskIntoConstraints = false
        button_demo_button.layer.cornerRadius = 10
        button_demo_button.clipsToBounds = true
        button_demo_button.backgroundColor = UIColor.green
        button_demo_button.setTitleColor( UIColor.white, for: .normal )
        button_demo_button.addTarget( nil, action: #selector( AnotherViewController.demo_button_pressed ),
                                      for: .touchUpInside )
        button_demo_button.widthAnchor.constraint( equalToConstant: 192 ).isActive = true
        button_demo_button.heightAnchor.constraint( equalToConstant: 96 ).isActive = true

        view.addSubview( back_button )
        view.addSubview( button_demo_button )
        
        back_button.centerXAnchor.constraint( equalTo: view.layoutMarginsGuide.centerXAnchor ).isActive = true
        
        back_button.centerYAnchor.constraint( equalTo: view.layoutMarginsGuide.centerYAnchor ).isActive = true
        
        button_demo_button.centerXAnchor.constraint( equalTo: view.layoutMarginsGuide.centerXAnchor ).isActive = true
        button_demo_button.centerYAnchor.constraint( equalTo: view.layoutMarginsGuide.centerYAnchor, constant: 112 ).isActive = true
    }
    
    func back_button_pressed()
    {
        presentingViewController!.dismiss( animated: true )
    }
    
    func demo_button_pressed()
    {
        
        button_demo_view_controller.modalTransitionStyle = .coverVertical
        button_demo_view_controller.modalPresentationStyle = .formSheet
        
        present( button_demo_view_controller, animated: true, completion: nil )
    }
    
    override func viewDidAppear( _ animated: Bool )
    {
        super.viewDidAppear( animated )
        
        print( "CYAN view did appear." )
    }
    
    override func viewWillDisappear( _ animated: Bool )
    {
        super.viewWillDisappear( animated )
        
        print( "CYAN view will disappear." )
    }
    
    // The following method is called when the device is rotated.
    
    override func viewWillTransition( to size: CGSize,
                                      with coordinator: UIViewControllerTransitionCoordinator )
    {
        super.viewWillTransition( to: size, with: coordinator )
        
        print( "CYAN view will transition." )
    }
    
}

