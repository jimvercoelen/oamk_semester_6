
//  AppDelegate.swift
//  ChangingViewsNoIB
//
//  Created by kari on 07/05/2017.
//  Copyright © 2017 kari. All rights reserved.

//  2017-09-11 Last modification.

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
   var window: UIWindow? = UIWindow( frame: UIScreen.main.bounds )
   
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
   {
      // This app could be made also in the following way, so that the
      // MainViewController is 'inside' a UINavigationController. At the moment
      // I do not know what might be the benefits of this.
      
      // let navigation_controller = UINavigationController( rootViewController: MainViewController() )
      
      // navigation_controller.setNavigationBarHidden( true, animated: false )
      
      // window?.rootViewController = navigation_controller
      
      window?.rootViewController = MainViewController()
      
      window?.makeKeyAndVisible()
      
      return true
   }
}

