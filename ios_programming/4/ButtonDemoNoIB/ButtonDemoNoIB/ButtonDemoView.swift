//
//  ButtonDemoView.swift
//  ButtonDemoNoIB
//
//  Created by kari on 10/03/2017.
//  Copyright © 2017 kari. All rights reserved.
//  2017-08-21 Last modification.

/* This version of the ButtonDemo app is created without the Interface
 Builder.
 
 When the project was created:
 Main.storyboard was deleted
 Main interface field was emptied.
 
 As the Interface Builder is not in use, all characteristics
 of the UILabel and UIButton are specified here with Swift code.
 
 Constraints are specified to position these UI components.
 
 */



import UIKit


class ButtonDemoView: UIView
{
    let text_to_show: UILabel
    let button_to_press: UIButton
    let button_back: UIButton
    
    override init( frame: CGRect )
    {
        text_to_show = UILabel()
        text_to_show.translatesAutoresizingMaskIntoConstraints = false
        text_to_show.textAlignment = .center
        text_to_show.text = "Hello!"
        text_to_show.font = UIFont( name: "Georgia", size: 36 )
        
        button_to_press = UIButton(type: .system)
        button_to_press.translatesAutoresizingMaskIntoConstraints = false
        button_to_press.setTitle("PRESS!", for: .normal)
        button_to_press.backgroundColor = UIColor.red
        button_to_press.setTitleColor( UIColor.white, for: .normal )
        button_to_press.addTarget( nil, action: #selector( ButtonDemoView.button_pressed ),
                                   for: .touchUpInside )
        
        
        button_back = UIButton(type: .system)
        button_back.translatesAutoresizingMaskIntoConstraints = false
        button_back.setTitle("BACK!", for: .normal)
        button_back.backgroundColor = UIColor.red
        button_back.setTitleColor( UIColor.white, for: .normal )
        button_back.addTarget( nil, action: #selector( ButtonDemoViewController.back_button_pressed ),
                                   for: .touchUpInside )
        

        super.init( frame: frame )
        
        backgroundColor = UIColor( red: 0.9, green: 1.0, blue: 1.0, alpha: 1.0 )
        
        addSubview( text_to_show )
        addSubview( button_to_press )
        addSubview( button_back )
        
        text_to_show.topAnchor.constraint( equalTo: layoutMarginsGuide.topAnchor, constant: 96 ).isActive = true
        text_to_show.leadingAnchor.constraint( equalTo: layoutMarginsGuide.leadingAnchor).isActive = true
        text_to_show.trailingAnchor.constraint( equalTo: layoutMarginsGuide.trailingAnchor).isActive = true
        
        button_to_press.widthAnchor.constraint( equalToConstant: 96 ).isActive = true
        button_to_press.heightAnchor.constraint( equalToConstant: 64 ).isActive = true
        button_to_press.centerXAnchor.constraint( equalTo: text_to_show.centerXAnchor ).isActive = true
        button_to_press.topAnchor.constraint( equalTo: text_to_show.bottomAnchor, constant: 64 ).isActive = true
        
        
        button_back.widthAnchor.constraint( equalToConstant: 96 ).isActive = true
        button_back.heightAnchor.constraint( equalToConstant: 64 ).isActive = true
        button_back.centerXAnchor.constraint( equalTo: button_to_press.centerXAnchor ).isActive = true
        button_back.topAnchor.constraint( equalTo: button_to_press.bottomAnchor, constant: 64 ).isActive = true
        
    }
    
    convenience required init(coder aDecoder: NSCoder)
    {
        self.init()
    }
    
    func button_pressed()
    {
        
        if text_to_show.text == "Hello!"
        {
            text_to_show.text = "Well done!"
        }
        else
        {
            text_to_show.text = "Hello!"
        }
    }
    
}

