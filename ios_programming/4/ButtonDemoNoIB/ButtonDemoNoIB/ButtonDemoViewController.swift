//
//  ButtonDemoViewController.swift Copyright Kari Laitinen
//  ButtonDemoNoIB
//
//  2017-03-10 Created by Kari.
//  Copyright © 2017 kari. All rights reserved.
//  2017-08-21 Last modification.

import UIKit

class ButtonDemoViewController: UIViewController
{
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView()
    {
        view = ButtonDemoView()
    }
    
    func back_button_pressed()
    {
        presentingViewController!.dismiss( animated: true )
    }
}

