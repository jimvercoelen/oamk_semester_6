//
//  AppDelegate.swift
//  ButtonDemoNoIB
//
//  Created by kari on 10/03/2017.
//  Copyright © 2017 kari. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
   var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
   
   func application(_ application: UIApplication,
                    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
   {
      
      window?.rootViewController = ButtonDemoViewController()
      window?.makeKeyAndVisible()
      
      return true
   }
}

