package App;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main implements Observer {
  private ExecutorService executor;
  private Scanner scanner;
  private String input;
  private int counter;

  public static void main(String[] args) {
    new Main();
  }

  private Main() {
    executor = Executors.newFixedThreadPool(100);
    scanner = new Scanner(System.in);
    input = null;

    System.out.println("Press x to start a thread or anything els to quit the program");

    while (true) {
      input = scanner.nextLine();

      if (input.equals("x")) {
        CounterRunnable counterRunnable = new CounterRunnable(counter++);
        counterRunnable.addObserver(this);
        Thread thread = new Thread(counterRunnable);
        executor.execute(thread);
      } else {
        System.out.println("Good by");
        System.exit(0);
      }
    }
  }

  @Override
  public void update(Observable o, Object message) {
    System.out.println(message);
  }

}
