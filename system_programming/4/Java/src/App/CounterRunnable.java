package App;

import java.util.Observable;

public class CounterRunnable extends Observable implements Runnable {
  private volatile boolean stop;
  private int id;
  private int process;

  public CounterRunnable(int id){
    this.stop = false;
    this.id = id;
    this.process = 0;
  }

  @Override
  public void run() {
    while (!stop) {
      String message = "* Thread " + id + " - on process: " + process + "%";

      try {
        process += 10;

        setChanged();
        notifyObservers(message);

        if (process > 100){
          return;
        }

        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
