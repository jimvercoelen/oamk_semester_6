package jimvercoelen.weatherapp;

import android.app.Application;
import android.content.Context;

import jimvercoelen.weatherapp.component.AppComponent;
import jimvercoelen.weatherapp.component.DaggerAppComponent;
import jimvercoelen.weatherapp.modules.AppModule;

public class App extends Application {
  private AppComponent appComponent;

  @Override
  public void onCreate() {
    super.onCreate();

    appComponent = DaggerAppComponent.builder()
      .appModule(new AppModule(this))
      .build();
  }

  public static AppComponent getComponent(Context context) {
    return ((App) context.getApplicationContext()).appComponent;
  }
}
