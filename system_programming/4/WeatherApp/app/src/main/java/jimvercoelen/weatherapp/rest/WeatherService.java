package jimvercoelen.weatherapp.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

public class WeatherService {

  private WeatherService() {}

  public static WeatherApi createWeatherService() {
    Retrofit.Builder builder = new Retrofit.Builder();
    builder.baseUrl("http://api.openweathermap.org");
    builder.addConverterFactory(GsonConverterFactory.create());
    builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());

    return builder.build().create(WeatherApi.class);
  }
}
