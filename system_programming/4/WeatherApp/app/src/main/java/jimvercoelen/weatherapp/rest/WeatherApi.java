package jimvercoelen.weatherapp.rest;

import jimvercoelen.weatherapp.models.WeatherData;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface WeatherApi {
  @GET("data/2.5/weather")
  Observable<WeatherData> getWeather3(@Query("q") String city, @Query("APPID") String APIid);
}
