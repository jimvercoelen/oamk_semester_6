package jimvercoelen.weatherapp.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jimvercoelen.weatherapp.observables.ObservableWeather;
import jimvercoelen.weatherapp.rest.WeatherApi;
import jimvercoelen.weatherapp.rest.WeatherService;

@Module
public final class AppModule {
  Application application;

  public AppModule(Application application) {
    this.application = application;
  }

  @Provides @Singleton
  Application provideApplication() {
    return this.application;
  }

  @Provides @Singleton
  WeatherApi provideClient() {
    return WeatherService.createWeatherService();
  }

  @Provides @Singleton
  ObservableWeather provideObservableWeather() {
    return new ObservableWeather();
  }
}
