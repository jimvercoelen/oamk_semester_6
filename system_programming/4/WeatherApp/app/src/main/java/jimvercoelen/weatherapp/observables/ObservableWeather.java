package jimvercoelen.weatherapp.observables;

import jimvercoelen.weatherapp.models.WeatherData;
import jimvercoelen.weatherapp.rest.WeatherApi;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public class ObservableWeather {
  private static String APIID = "65dbec3aae5e5bf9000c7a956c8b76f6";

  public ObservableWeather () {

  }

  public Observable<WeatherData> getWeather(WeatherApi client, String city) {
    BehaviorSubject<WeatherData> requestSubject = BehaviorSubject.create();

    client.getWeather3(city, APIID)
      .subscribeOn(Schedulers.io())
      .observeOn(Schedulers.io())
      .subscribe(
        requestSubject::onNext,
        requestSubject::onError,
        requestSubject::onCompleted
      );

    return  requestSubject.asObservable();
  }
}
