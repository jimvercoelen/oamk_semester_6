package jimvercoelen.weatherapp.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jimvercoelen.weatherapp.App;
import jimvercoelen.weatherapp.R;
import jimvercoelen.weatherapp.models.WeatherData;
import jimvercoelen.weatherapp.observables.ObservableWeather;
import jimvercoelen.weatherapp.rest.WeatherApi;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = MainActivity.class.getName();
  private String city;

  @Inject WeatherApi client;
  @Inject ObservableWeather observableWeather;
  @BindView(R.id.edit_city) EditText cityEditText;
  @BindView(R.id.text_temperature) TextView temperatureTextView;
  @BindView(R.id.image_weather) ImageView weatherImageView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    App.getComponent(this).inject(this);
    ButterKnife.bind(this);

    city = getCityFromSharedPrefs();
    if (city != null && !city.isEmpty()) {
      cityEditText.setText(city);
      getWeather(city);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    return id == R.id.action_settings || super.onOptionsItemSelected(item);
  }

  @OnClick(R.id.button_get_weather)
  public void onGetWeatherClick(View view) {
    String city = cityEditText.getText().toString();

    if (city.isEmpty()) {
      return;
    }

    setCityToSharedPrefs(city);

    getWeather(city);
  }

  private String getCityFromSharedPrefs() {
    SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
    return sharedPref.getString(getString(R.string.shared_prefs_city), null);
  }

  private void setCityToSharedPrefs(String city) {
    SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putString(getString(R.string.shared_prefs_city), city);
    editor.apply();
  }

  private void getWeather(String city) {
    observableWeather
      .getWeather(client, city)
      .subscribeOn(Schedulers.newThread())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onSuccess,
        this::onError,
        this::onComplete
      );
  }

  private void onSuccess(WeatherData weatherData) {
    Log.i(TAG, "received: " + weatherData.toString());
    String formatted = String.format(getString(R.string.temp), String.valueOf(weatherData.getMain().getTemp()));
    String weatherIcon = weatherData.getWeather().get(0).getIcon(); // Always on index 0
    temperatureTextView.setText(formatted);
    Picasso.with(this).load("http://openweathermap.org/img/w/" + weatherIcon + ".png").into(weatherImageView);
  }

  private void onError(Throwable error) {
    Log.e(TAG, error.getMessage());
    Log.getStackTraceString(error);
  }

  private void onComplete() {
    Log.i(TAG, "Completed getting weather");
  }
}
