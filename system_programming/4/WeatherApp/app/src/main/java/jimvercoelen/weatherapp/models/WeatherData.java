package jimvercoelen.weatherapp.models;

import java.util.List;

public class WeatherData {
  private Coord coord;
  private List<Weather> weather;
  private String base;
  private Main main;
  private int visibility;
  // wind
  // clouds
  private int dt;
  // sys
  private int id;
  private String name;
  private int cod;

  public WeatherData(Coord coord, List<Weather> weather, Main main, String base, int visibility, int dt, int id, String name, int cod) {
    this.coord = coord;
    this.weather = weather;
    this.base = base;
    this.main = main;
    this.visibility = visibility;
    this.dt = dt;
    this.id = id;
    this.name = name;
    this.cod = cod;
  }

  public Coord getCoord() {
    return coord;
  }

  public List<Weather> getWeather() {
    return weather;
  }

  public String getBase() {
    return base;
  }

  public Main getMain() {
    return main;
  }

  public int getVisibility() {
    return visibility;
  }

  public int getDt() {
    return dt;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getCod() {
    return cod;
  }

  @Override
  public String toString() {
    return "coord: " + coord.toString() +
      " weather: " + weather.toString() +
      " base: " + base +
      " main: " + main +
      " visibility: " + visibility +
      " dt: " + dt +
      " id: " + id +
      " name: " + name +
      " cod: " + cod;
  }

  public class Coord {
    private double lon;
    private double lat;

    public Coord(double lon, double lat) {
      this.lon = lon;
      this.lat = lat;
    }

    public double getLon() {
      return lon;
    }

    public double getLat() {
      return lat;
    }

    @Override
    public String toString() {
      return "lon: " + lon + " lat: " + lat;
    }
  }

  public class Weather {
    private int id;
    private String main;
    private String description;
    private String icon;

    public Weather(int id, String main, String description, String icon) {
      this.id = id;
      this.main = main;
      this.description = description;
      this.icon = icon;
    }

    public int getId() {
      return id;
    }

    public String getMain() {
      return main;
    }

    public String getDescription() {
      return description;
    }

    public String getIcon() {
      return icon;
    }

    @Override
    public String toString() {
      return "id: " + id + " main: " + main + " description: " + description + " icon: " + icon;
    }
  }

  public class Main {
    private double temp;
    private int pressure;
    private int humidity;
    private double temp_min;
    private double temp_max;

    public Main(double temp, int pressure, int humidity) {
      this.temp = temp;
      this.pressure = pressure;
      this.humidity = humidity;
    }

    public Main(double temp, int pressure, int humidity, double temp_min, double temp_max) {
      this.temp = temp;
      this.pressure = pressure;
      this.humidity = humidity;
      this.temp_min = temp_min;
      this.temp_max = temp_max;
    }

    public double getTemp() {
      return temp;
    }

    public int getPressure() {
      return pressure;
    }

    public int getHumidity() {
      return humidity;
    }

    public double getTempMin() {
      return temp_min;
    }

    public double getTempMax() {
      return temp_max;
    }

//    @Override
//    public String toString() {
//      return "temp: " + temp + " pressure: " + pressure + " humidity: " + humidity;
//    }

    @Override
    public String toString() {
      return "temp: " + temp + " pressure: " + pressure + " humidity: " + humidity + " temMin: " + temp_min + " temp_max: " + temp_max;
    }
  }
}
