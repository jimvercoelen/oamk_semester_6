package jimvercoelen.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements Observer {
  private static String TAG = MainActivity.class.getName();
  private ExecutorService executor;
  private int counter;
  private List<String> threadsList;
  private ArrayAdapter<String> threadsAdapter;
  @BindView(R.id.list_thread_monitor) ListView monitorListView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);

    executor = Executors.newFixedThreadPool(100);
    threadsList = new ArrayList<>();
    threadsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, threadsList);
    monitorListView.setAdapter(threadsAdapter);
  }

  @OnClick(R.id.button_start_thread)
  public void onStartThreadClick(View view) {
    CounterRunnable counterRunnable = new CounterRunnable(counter++);
    counterRunnable.addObserver(this);
    Thread thread = new Thread(counterRunnable);
    executor.execute(thread);
  }

  @Override
  public void update(Observable observable, final Object message) {
    Log.i(TAG, message.toString());
    runOnUiThread(() -> threadsAdapter.add(message.toString()));
  }
}
