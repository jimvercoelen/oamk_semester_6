package App;

import java.io.*;
import java.util.Scanner;

public class Main {
  private static final String PATH = "/Users/jimvercoelen/Documents/school/semester6/oamk/system_programming/5/JavaWriteRead/public/fileToRead.txt";

  public static void main(String[] args) {
    run();
  }

  private static void run() {
    Scanner scanner = new Scanner(System.in);

    readFile();

    while (true) {
      System.out.println("Enter something to write to file");
      String input = scanner.nextLine();

      if (input != null || !input.isEmpty()) {
        System.out.println("Writing to file: " + input);
        writeToFile(input);
      }
    }
  }

  private static void readFile() {
    try {
      Scanner in = new Scanner(new FileReader(PATH));
      StringBuilder stringBuilder = new StringBuilder();

      while (in.hasNext()) {
        stringBuilder.append(in.next());
      }

      in.close();
      System.out.println(stringBuilder.toString());
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static void writeToFile(String input) {
    FileWriter fileWriter = null;
    BufferedWriter bufferedWriter = null;

    try {
      fileWriter = new FileWriter(PATH, true);
      bufferedWriter = new BufferedWriter(fileWriter);
      bufferedWriter.write(input + "\n");
      bufferedWriter.close();
      fileWriter.close();
      System.out.println("Done writing to file");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
