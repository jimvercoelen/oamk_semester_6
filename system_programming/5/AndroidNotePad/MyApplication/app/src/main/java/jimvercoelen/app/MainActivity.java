package jimvercoelen.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

  @BindView(R.id.text_input) EditText textInput;
  @BindView(R.id.text_note_pad) TextView textNote;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);

    textNote.setText(getNoteFromSharedPrefs());
  }

  @OnClick(R.id.button_save)
  public void onSaveClick(View view) {
    String input = textInput.getText().toString();
    String newNote = input + "\n" + getNoteFromSharedPrefs();
    setNoteToSharedPrefs(newNote);

    textNote.setText(newNote);
    textInput.setText("");
  }

  private String getNoteFromSharedPrefs() {
    SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
    return sharedPref.getString(getString(R.string.shared_prefs_input), null);
  }

  private void setNoteToSharedPrefs(String input) {
    SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putString(getString(R.string.shared_prefs_input), input);
    editor.apply();
  }
}
