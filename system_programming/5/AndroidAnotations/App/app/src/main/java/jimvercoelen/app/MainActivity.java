package jimvercoelen.app;

import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
  private int backgroundCounter = 0;

  @ViewById Button buttonCounter;
  @ViewById TextView textCounter;

  @Click({ R.id.buttonCounter })
  public void onButtonCounterClick() {
    this.doBackgroundSomething();
  }

  @Background
  public void doBackgroundSomething() {
    new Thread(() -> {
      try {
        Thread.sleep(1000);
        this.updateUI(backgroundCounter++);
//        runOnUiThread(() -> this.updateUI(backgroundCounter++));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }).start();
  }

  @UiThread
  public void updateUI(int counter) {
    textCounter.setText(String.valueOf(counter));
  }
}
