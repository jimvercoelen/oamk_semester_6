package jimvercoelen.threadapp;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = MainActivity.class.getName();
  private Thread thread;
  private Handler handler;

  @BindView(R.id.text_counter) TextView tvCounter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);
  }

  @OnClick(R.id.button_start)
  public void onStartClick(View view) {
    handler = new Handler();

    thread = newCounterThread();
    thread.start();

//    counter += 1;
//
//    Single.just(counter)
//      .subscribeOn(Schedulers.newThread())
//      .delay(2, java.util.concurrent.TimeUnit.SECONDS, AndroidSchedulers.mainThread())
//      .subscribe(newCount -> tvCounter.setText(newCount.toString()));
  }

  @OnClick(R.id.button_stop)
  public void onStopClick(View view) {
    thread.interrupt();
  }

  private Thread newCounterThread() {
    return new Thread(() -> {
      int counter = 0;

      while (true) {
        try {
          counter += 1;
          Thread.sleep(2000);
          int finalCounter = counter;
          handler.post(() -> tvCounter.setText(String.valueOf(finalCounter)));
        } catch (Exception e) {
          Thread.currentThread().interrupt();
        }
      }
    });
  }
}
