package app;

import java.util.Scanner;

public class Main {
  private static Scanner scanner;
  private static String input;
  private static Thread thread;

  static {
    scanner = new Scanner(System.in);
    input = null;
  }

  public static void main(String[] args) {
    while (true) {
      System.out.println("Press START, STOP OR QUIT");
      input = scanner.nextLine();

      switch (input) {
        case "START": {
          System.out.println("Thread starting printing stuff");
          thread = newThread();
          thread.start();
          break;
        }
        case "STOP": {
          System.out.println("Thread stopped printing stuff");
          thread.interrupt();
          break;
        }
        case "QUIT": {
          System.out.println("Good by");
          System.exit(0);
          break;
        }
        default: {
          System.out.println("Unknown command");
          break;
        }
      }
    }
  }

  static private Thread newThread() {
    return new Thread(() -> {
      try {
        while (true) {
          System.out.println("Tiisu, We want More!");
          Thread.sleep(3000);
        }
      } catch (Exception e) {
        Thread.currentThread().interrupt();
      }
    });
  }
}
