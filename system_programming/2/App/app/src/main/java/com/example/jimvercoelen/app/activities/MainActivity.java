package com.example.jimvercoelen.app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.jimvercoelen.app.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = MainActivity.class.getName();

  @BindView(R.id.list_log) ListView listLog;
  private List<String> log;
  private ArrayAdapter<String> logAdapter;

  // User navigates to the activity
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);

    log = new ArrayList<>();
    logAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, log);
    listLog.setAdapter(logAdapter);

    logActivityStatus("On create");
  }

  @Override
  protected void onStart() {
    super.onStart();

    logActivityStatus("On start");
  }

  // User navigates to the activity
  @Override
  protected void onRestart() {
    super.onRestart();

    logActivityStatus("On restart");
  }

  // User returns to activity
  @Override
  protected void onResume() {
    super.onResume();

    logActivityStatus("On resume");
  }

  // Another activity comes into the foreground
  @Override
  protected void onPause() {
    super.onPause();

    logActivityStatus("On pause");
  }

  // The activity is no longer visible
  @Override
  protected void onStop() {
    super.onStop();

    logActivityStatus("On stop");
  }

  // The activity is finished or being destroyed by the system
  @Override
  protected void onDestroy() {
    super.onDestroy();

    logActivityStatus("On destroy");
  }

  private void logActivityStatus(String status) {
    Log.i(TAG, status);
    log.add(TAG + " " + status);
    logAdapter.notifyDataSetChanged();
  }
}
