package com.example.jimvercoelen.app.application;

import android.app.Application;
import android.util.Log;

public class App extends Application {
  private static final String TAG = App.class.getName();

  @Override
  public void onCreate() {
    super.onCreate();

    Log.i(TAG, "on create");
  }
}
