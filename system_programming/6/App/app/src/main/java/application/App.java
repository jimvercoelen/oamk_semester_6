package application;

import android.app.Application;
import android.content.Context;

import component.AppComponent;
import component.DaggerAppComponent;
import modules.AppModule;

public class App extends Application {
  private AppComponent appComponent;

  @Override
  public void onCreate() {
    super.onCreate();

    appComponent = DaggerAppComponent.builder()
      .appModule(new AppModule(this))
      .build();
  }

  public static AppComponent getComponent(Context context) {
    return ((App) context.getApplicationContext()).appComponent;
  }
}
