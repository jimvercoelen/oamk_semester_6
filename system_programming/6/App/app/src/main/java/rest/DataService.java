package rest;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataService {
  private static final String BASE_URL = "http://www.amica.fi/";
  private DataService() {}

  public static DataApi createDataService() {
    Retrofit.Builder builder = new Retrofit.Builder();
    builder.baseUrl(BASE_URL);
    builder.addConverterFactory(GsonConverterFactory.create());
    builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());

    return builder.build().create(DataApi.class);
  }
}
