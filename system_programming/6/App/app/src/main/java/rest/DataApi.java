package rest;

import modals.Data;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface DataApi {
  @GET("api/restaurant/menu/day")
  Observable<Data> getData(@Query("date") String data, @Query("language") String language, @Query("restaurantPageId") int restaurantPageId);
}
