package activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import application.App;
import butterknife.BindView;
import butterknife.ButterKnife;
import jimvercoelen.app.R;
import modals.Data;
import observables.ObservableData;
import rest.DataApi;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = MainActivity.class.getName();

  @Inject DataApi client;
  @Inject ObservableData observableData;
  @BindView(R.id.menuList) ListView menuList;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    App.getComponent(this).inject(this);
    ButterKnife.bind(this);

    getData();
  }

  private void getData() {
    observableData
      .getData(client)
      .subscribeOn(Schedulers.newThread())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onSuccess,
        this::onError,
        this::onComplete
      );
  }

  private void onSuccess(Data data) {
    Log.i(TAG, "received data");
    List<String> menuItems = new ArrayList<>();

    for (Data.SetMenu setMenu : data.getLunchMenu().getSetMenus()) {
      for (Data.Meal meal: setMenu.getMeals()) {
        menuItems.add(meal.toString());
      }
    }

    ArrayAdapter<String> listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, menuItems);
    menuList.setAdapter(listAdapter);
  }

  private void onError(Throwable error) {
    Log.e(TAG, error.getMessage());
    Log.getStackTraceString(error);
  }

  private void onComplete() {
    Log.i(TAG, "Completed getting data");
  }
}
