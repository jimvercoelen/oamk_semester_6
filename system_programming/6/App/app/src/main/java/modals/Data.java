package modals;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
  @SerializedName("LunchMenu")
  private LunchMenu lunchMenu;

  public Data(LunchMenu lunchMenu) {
    this.lunchMenu = lunchMenu;
  }

//  @SerializedName("RequireDietFilters")
//  private RequireDietFilters requireDietFilters;
//
//  @SerializedName("ExcludeDietFilters")
//  private ExcludeDietFilters excludeDietFilters;
//
//  @SerializedName("RestaurantDietFilters")
//  private RestaurantDietFilters restaurantDietFilters;
//
//  public Data(LunchMenu lunchMenu,
//              RequireDietFilters requireDietFilters,
//              ExcludeDietFilters excludeDietFilters,
//              RestaurantDietFilters restaurantDietFilters) {
//    this.lunchMenu = lunchMenu;
//    this.requireDietFilters = requireDietFilters;
//    this.excludeDietFilters = excludeDietFilters;
//    this.restaurantDietFilters = restaurantDietFilters;
//  }

  public LunchMenu getLunchMenu() {
    return lunchMenu;
  }

//  public RequireDietFilters getRequireDietFilters() {
//    return requireDietFilters;
//  }
//
//  public ExcludeDietFilters getExcludeDietFilters() {
//    return excludeDietFilters;
//  }
//
//  public RestaurantDietFilters getRestaurantDietFilters() {
//    return restaurantDietFilters;
//  }

  public class LunchMenu {
    @SerializedName("DayOfWeek")
    private String dayOfWeek;

    @SerializedName("Date")
    private String date;

    @SerializedName("SetMenus")
    private List<SetMenu> setMenus;

    public LunchMenu(String dayOfWeek, String date, List<SetMenu> setMenus) {
      this.dayOfWeek = dayOfWeek;
      this.date = date;
      this.setMenus = setMenus;
    }

    public String getDayOfWeek() {
      return dayOfWeek;
    }

    public String getDate() {
      return date;
    }

    public List<SetMenu> getSetMenus() {
      return setMenus;
    }
  }

  public class SetMenu {
    @SerializedName("SortOrder")
    private int sortOrder;

    @SerializedName("Name")
    private String name;

    @SerializedName("Price")
    private double price;

    @SerializedName("Meals")
    private List<Meal> meals;

    public SetMenu(int sortOrder, String name, double price, List<Meal> meals) {
      this.sortOrder = sortOrder;
      this.name = name;
      this.price = price;
      this.meals = meals;
    }

    public int getSortOrder() {
      return sortOrder;
    }

    public String getName() {
      return name;
    }

    public double getPrice() {
      return price;
    }

    public List<Meal> getMeals() {
      return meals;
    }
  }

  public class Meal {
    @SerializedName("Name")
    private String name;

    @SerializedName("RecipeId")
    private int recipeId;

    @SerializedName("Diets")
    private List<String> diets;

    @SerializedName("Nutrients")
    private String nutrients;

    public Meal(String name, int recipeId, List<String> diets, String nutrients) {
      this.name = name;
      this.recipeId = recipeId;
      this.diets = diets;
      this.nutrients = nutrients;
    }

    public String getName() {
      return name;
    }

    public int getRecipeId() {
      return recipeId;
    }

    public List<String> getDiets() {
      return diets;
    }

    public String getNutrients() {
      return nutrients == null ? "Unknown" : nutrients;
    }

    @Override
    public String toString () {
      return "name: " + name +
        "\ndiets: " + diets.toString() +
        "\nnutrients: " + nutrients;

    }
  }

  public class RequireDietFilters {
    // TODO
  }

  public class ExcludeDietFilters {
    // TODO
  }

  public class RestaurantDietFilters {
    // TODO
  }
}
