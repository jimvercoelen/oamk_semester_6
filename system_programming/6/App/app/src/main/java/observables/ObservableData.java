package observables;

import modals.Data;
import rest.DataApi;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public class ObservableData {
  private static final String DATE = "2017-10-3";             // TODO get todays day
  private static final String LANGUAGE = "en";
  private static final int RESTAURANT_PAGE_ID = 66287;

  public ObservableData() {

  }

  public Observable<Data> getData(DataApi client) {
    BehaviorSubject<Data> requestSubject = BehaviorSubject.create();

    client.getData(DATE, LANGUAGE, RESTAURANT_PAGE_ID)
      .subscribeOn(Schedulers.io())
      .observeOn(Schedulers.io())
      .subscribe(
        requestSubject::onNext,
        requestSubject::onError,
        requestSubject::onCompleted
      );

    return  requestSubject.asObservable();
  }
}
