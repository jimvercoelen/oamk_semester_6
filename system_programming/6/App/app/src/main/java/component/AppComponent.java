package component;

import javax.inject.Singleton;

import activities.MainActivity;
import dagger.Component;
import modules.AppModule;

@Singleton
@Component(
  modules = {
    AppModule.class
  }
)
public interface AppComponent {
  void inject(MainActivity activity);
}
