package jimvercoelen.androidapp;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = MainActivity.class.getName();
  private Handler handler;
  private String text;

  @BindView(R.id.edit_url) EditText editUrl;
  @BindView(R.id.text_result) TextView textResult;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    handler = new Handler();

    ButterKnife.bind(this);
  }

  @OnClick(R.id.button_fetch)
  public void onFetchClick(View view) {
    new Thread(() -> {
      handler.post(() -> textResult.setText(""));

      try {
        String targetUrl = editUrl.getText().toString();
        URL url = new URL(targetUrl);
        URLConnection connection = url.openConnection();
        InputStream responseStream = connection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(responseStream));
        String line;
        StringBuilder response = new StringBuilder();

        while ((line = br.readLine()) != null) {
          response.append(line);
        }

        handler.post(() -> textResult.setText(response.toString()));

        br.close();
      } catch (Exception e) {
        Log.e(TAG, e.getMessage());
        e.printStackTrace();
      }
    }).start();

    //    new FetchDataTask().execute("url");
  }

//  class FetchDataTask extends AsyncTask<String, Void, StringBuilder> {
//    private StringBuilder response;
//
//    @Override
//    protected StringBuilder doInBackground(String... strings) {
//      try {
//        response = new StringBuilder();
//        URL url = new URL("http://yahoo.com");
//        URLConnection yc = url.openConnection();
//        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
//        String input;
//
//        while ((input = in.readLine()) != null) {
//          System.out.println(input);
//          response.append(input);
//        }
//
//        in.close();
//      } catch (Exception e) {
//        e.printStackTrace(System.out);
//      }
//
//      return response;
//    }
//
//    protected void onPostExecute(StringBuilder response) {
//      System.out.println(response);
//    }
//  }
}
