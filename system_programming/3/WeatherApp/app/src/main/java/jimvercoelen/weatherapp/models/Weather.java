package jimvercoelen.weatherapp.models;

public class Weather {
  private String id;
  private String city;
  private String icon;
  private int temperature;

  public Weather(String id, String city, int temperature, String icon) {
    this.id = id;
    this.city = city;
    this.temperature = temperature;
    this.icon = icon;
  }

  public String getId() {
    return id;
  }

  public String getCity() {
    return city;
  }

  public int getTemperature() {
    return temperature;
  }

  public String getIcon() {
    return icon;
  }

  @Override
  public String toString() {
    return "id: " + id + ", city: " + city + ",  temperature:" + temperature + ", icon: " + icon;
  }
}
