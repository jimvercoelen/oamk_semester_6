package jimvercoelen.weatherapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jimvercoelen.weatherapp.App;
import jimvercoelen.weatherapp.R;
import jimvercoelen.weatherapp.models.Weather;
import jimvercoelen.weatherapp.observables.ObservableWeather;
import jimvercoelen.weatherapp.rest.WeatherApi;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = MainActivity.class.getName();

  @Inject WeatherApi client;
  @Inject ObservableWeather observableWeather;
  @BindView(R.id.edit_city) EditText etCity;
  @BindView(R.id.text_temperature) TextView tvTemperature;
  @BindView(R.id.image_weather) ImageView ivWeather;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    App.getComponent(this).inject(this);
    ButterKnife.bind(this);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    return id == R.id.action_settings || super.onOptionsItemSelected(item);
  }

  @OnClick(R.id.button_get_weather)
  public void onGetWeatherClick(View view) {
    String city = etCity.getText().toString();

    observableWeather
      .getWeather(client, city)
      .subscribeOn(Schedulers.newThread())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(
        this::onSuccess,
        this::onError,
        this::onComplete
      );
  }

  private void onSuccess(Weather weather) {
    Log.i(TAG, "received: " + weather.toString());
    String formatted = String.format(getString(R.string.temp), String.valueOf(weather.getTemperature()));
    tvTemperature.setText(formatted);
    Picasso.with(this).load("http://openweathermap.org/img/w/" + weather.getIcon() + ".png").into(ivWeather);
  }

  private void onError(Throwable error) {
    Log.e(TAG, error.getMessage());
    Log.getStackTraceString(error);
  }

  private void onComplete() {
    Log.i(TAG, "Completed getting weather");
  }
}
