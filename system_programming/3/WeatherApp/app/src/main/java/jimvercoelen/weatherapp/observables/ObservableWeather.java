package jimvercoelen.weatherapp.observables;

import jimvercoelen.weatherapp.models.Weather;
import jimvercoelen.weatherapp.rest.WeatherApi;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

public class ObservableWeather {
  public ObservableWeather () {

  }

  public Observable<Weather> getWeather(WeatherApi client, String city) {
    BehaviorSubject<Weather> requestSubject = BehaviorSubject.create();

    client.getWeather(city)
      .subscribeOn(Schedulers.io())
      .observeOn(Schedulers.io())
      .subscribe(
        requestSubject::onNext,
        requestSubject::onError,
        requestSubject::onCompleted
      );

    return  requestSubject.asObservable();
  }
}
