package jimvercoelen.weatherapp.rest;

import jimvercoelen.weatherapp.models.Weather;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface WeatherApi {
  @GET("weather/{city}")
  Observable<Weather> getWeather(@Path("city") String city);
}
