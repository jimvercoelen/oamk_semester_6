import sun.misc.Cache;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class main {

  private static Scanner scanner;
  private static String input;

  static {
    scanner = new Scanner(System.in);
    input = null;
  }

  public static void main(String[] args) {
    while (true) {
      System.out.println("Enter a url to fetch ...");
      input = scanner.nextLine();

      executePost(input);
    }
  }

  private static void executePost(String targetUrl) {
    new Thread(() -> {
      try {
        URL url = new URL(targetUrl);
        URLConnection yc = url.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null)
          System.out.println(inputLine);
        in.close();
      } catch (Exception e) {
        e.printStackTrace(System.out);
      }
    }).run();
  }
}
